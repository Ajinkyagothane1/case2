#include<iostream>
#include<string.h>
#include<fstream>
#include<sstream>
#include <ostream>
#include<vector>

using namespace std;
class student
{
public:
		int id;
		string name;
		int rank_a;
		int rank_b;
		int rank_c;
		double avg;
		string branch;
		int p1;
		string n1;
		string n2;
		int p2;
		int p3;
		string n3;
public:
			student()
			{
				id=0;
				name ="";
				rank_a=0;
				rank_b=0;
				rank_c=0;
				avg=0.0;
				branch="";
				p1=0;
				n1 ="";
				n2 ="";
				p2=0;
				p3=0;
				n3 ="";

			}
			student(int sid, string sname, int sa, int sb, int sc, double savg,string sbranch, int sp1,string sn1,string sn2, int sp2, int sp3, string sn3)
			{
				id=sid;
				name=sname;
				rank_a=sa;
				rank_b=sb;
				rank_c=sc;
				avg=savg;
				branch=sbranch;
				p1=sp1;
				n1=sn1;
				n2=sn2;
				p2=sp2;
				p3=sp3;
				n3=sn3;
			}
			void set_id(int sid)
			{
				id=sid;
			}
			void set_name(string sname)
			{
				name=sname;
			}
			void set_rank_a(int sa)
			{
				rank_a=sa;
			}
			void set_rank_b(int sb)
			{
				rank_b=sb;
			}
			void set_rank_c(int sc)
			{
				rank_c=sc;
			}
			void set_avg(double savg)
			{
				avg=savg;
			}
			void set_branch(string sbranch)
			{
				branch=sbranch;
			}
			void set_p1(int sp1)
			{
				p1=sp1;
			}
			void set_n1(string sn1)
			{
				n1=sn1;
			}
			void set_n2(string sn2)
			{
				n2=sn2;
			}
			void set_p2(int sp2)
			{
				p2=sp2;
			}
			void set_p3(int sp3)
			{
				p3=sp3;
			}
			void set_n3(string sn3)
			{
				n3=sn3;
			}
			int get_id()
			{
				return id;
			}
			string get_name()
			{
				return name;
			}
			int get_a()
			{
				return rank_a;
			}
			int get_b()
			{
				return rank_b;
			}
			int get_c()
			{
				return rank_c;
			}
			double get_avg()
			{
				return avg;
			}
			string get_branch()
			{
				return branch;
			}
			int get_p1()
			{
				return p1;
			}
			string get_n1()
			{
				return n1;
			}
			string get_n2()
			{
				return n2;
			}
			int get_p2()
			{
				return p2;
			}
			int get_p3()
			{
				return p3;
			}
			string get_n3()
			{
				return n3;
			}
			void display()
			{
				cout<<id<<","<<name<<","<<rank_a<<","<<rank_b<<","<<rank_c<<","<<avg<<","<<branch<<","<<p1<<","<<n1<<","
				<<n2<<","<<p2<<","<<p3<<","<<n3<<endl;
			}
			~student()
			{

			}
};
class preferences
{
private:
	int form_no;
	int preference_no;
	string name;
	string center_name;
	vector<student> stud;
public:
	preferences()
	{
			form_no=0;
			preference_no=0;
			name="";
			center_name="";
	}
	preferences(int pform_no,int ppreference_no, string pname, string pcenter_name)
	{
			form_no=pform_no;
			preference_no=ppreference_no;
			name=pname;
			center_name=pcenter_name;
	}

		void set_form_no(int pform_no)
		{
			form_no=pform_no;
		}
		void set_preference_no(int ppreference_no)
		{
			preference_no=ppreference_no;

		}
		void set_name(string pname)
		{
			name=pname;
		}
		void set_center_id(string pcenter_name)
		{
			center_name=pcenter_name;
		}

		int get_form_no()
		{
			return form_no;
		}
		int get_preference_no()
		{
			return preference_no;
		}
		string get_name()
		{
			return name;
		}
		string get_center_id()
		{
			return center_name;
		}
		void display()
		{
			cout<<form_no<<","<<preference_no<<","<<name<<","<<center_name;
			cout<<endl;
		}
		void display_perference()
		{
				for(unsigned i=0; i<stud.size(); i++)
				{
					cout << " - ";
					stud[i].display();
				}
		}

};
void load_student(vector<student>& stud)
{
	ifstream fp;
	string line;
	int i;
	fp.open("students.csv");
	if(!fp)
	{
		cout<<"failed to open Students file"<<endl;
		return;
	}
	i=0;
	while(getline(fp,line))
	{
		stringstream str(line);
		string tokens[13];
		for(int i=0;i<13;i++)
		{
			getline(str,tokens[i],',');
		}
		student s(stoi(tokens[0]),tokens[1], stoi(tokens[2]), stoi(tokens[3]), stoi(tokens[4]), stod(tokens[5]), tokens[6], stoi(tokens[7]), tokens[8], tokens[9], stoi(tokens[10]), stoi(tokens[11]), tokens[12] );
		stud.push_back(s);
		i++;
	}
	fp.close();
	cout<<" Inserted successfully"<<i<<endl;
}
student* find_student(vector<student>& stud,int id1)
{
	unsigned i;
	for(i=0;i<stud.size();i++)
	{
		if(stud[i].get_id()==id1)
		{
			return &stud[i];
		}
	}
	return NULL;
}
void load_preference(vector<student>& stud)
{
	ifstream fp;
	string line;
	int i;
	fp.open("preferences.csv");
	if(!fp)
	{
		cout<<"failed to open perferences file"<<endl;
		return;
	}
	i=0;
	while(getline(fp,line))
	{
		stringstream str(line);
		string tokens[4];
		for(int i=0;i<4;i++)
		{
			getline(str,tokens[i],',');
		}
		preferences p(stoi(tokens[0]),stoi(tokens[1]),tokens[2],tokens[3]);
		student *s =find_student(stud,p.get_form_no());
		//s->pref.push_back(p);
		i++;
	}
	fp.close();
	cout<<" inserted successfully"<<i<<endl;
}
class cources
{
public:
	int num;
	string cource;
	int fees;
	string rank;
public:
	cources()
	{
		num=0;
		cource="";
		fees=0;
		rank="";
	}
	cources(int cnum, string ccource, int cfees, string crank)
	{
		num=cnum;
		cource=ccource;
		fees=cfees;
		rank=crank;
	}
	void set_num(int cnum)
	{
		num=cnum;
	}
	void set_cource(string ccource)
		{
			cource=ccource;
		}
	void set_fees(int cfees)
		{
			fees=cfees;
		}
	void set_rank(string crank)
		{
			rank=crank;
		}
	int get_num()
	{
		return num;
	}
	string get_cource()
		{
			return cource;
		}
	int get_fees()
	{
		return fees;
	}
	string get_rank()
	{
		return rank;
	}
	void display_cource()
	{
		cout<<num<<","<<cource<<","<<fees<<","<<rank<<endl;
	}
};
class eligibilities
{
public:
	string cource;
	string branch;
	double percentages;
	vector<cources> Cource;
public:
	eligibilities()
	{
		cource="";
		branch="";
		percentages=0.0;
	}
	eligibilities(string ecources, string ebranch, double epercentages)
	{
		cource=ecources;
		branch=ebranch;
		percentages=epercentages;
	}
	void set_cources(string ecources)
	{
		cource=ecources;
	}
	void set_branch(string ebranch)
		{
			branch=ebranch;
		}
	void set_percentages(int epercentages)
	{
		percentages=epercentages;
	}
	string get_cources()
	{
		return cource;
	}
	string get_branch()
	{
		return branch;
	}
	int get_percentages()
	{
		return percentages;
	}
	void display_cources()
	{
		cout<<cource<<","<<branch<<","<<percentages<<endl;
	}
	void display_Cource()
	{
			for(unsigned i=0; i<Cource.size(); i++)
			{
				cout << " - ";
				Cource[i].display_cource();
			}
		}

	};
void load_eligiblity_csv(vector<eligibilities>& eligi)
{
	ifstream fin("eligibilities.csv");
	if(!fin) {
		cout << "failed to open eligibilities file" << endl;
		return;
	}
	string line;
	while(getline(fin, line))
	{
		string tokens[3];
		stringstream str(line);
		for(int i=0; i<3; i++)
			getline(str, tokens[i], ',');
		eligibilities obj(tokens[0], tokens[1], stod(tokens[2]));
		eligi.push_back(obj);
	}
}
eligibilities * find_eligibility(vector<eligibilities>& eligi, string cource)
{
	unsigned i;
	for(i=0; i<eligi.size(); i++) {
		if(eligi[i].cource == cource)
			return &eligi[i];
	}
	return NULL;
}
void load_cource_csv(vector<eligibilities>& eligi)
{
	ifstream fin("courses.csv");
	if(!fin) {
		cout << "failed to open courses file" << endl;
		return;
	}
	string line;
	while(getline(fin, line))
	{
		string tokens[4];
		stringstream str(line);
		for(unsigned i=0; i<4; i++)
			getline(str, tokens[i], ',');
			cources obj(stoi(tokens[0]), tokens[1], stoi(tokens[2]), tokens[3]);
			eligibilities *e = find_eligibility(eligi, obj.cource);
			e->eligi.push_back(obj);
	}
}
int main()
{
	vector<student>stud;
	vector<preferences>perf;
	load_student(stud);
	load_preference(stud);
	cout<<"records are"<<endl;
	for(unsigned int i=0;i<stud.size();i++)
	{

		stud[i].display();
		//stud[i].display_perference()
		cout<<endl;
	}
return 0;
}
